class BigNumber:

    def __init__(self, value=0):
        if value==0:
            self.value = value
        else:
            self.value = int(value, 16)
            # self.value = self.setHex(value)


    def setHex(self, hexString):
        if isinstance(hexString, str) and hexString.isalnum() and \
        all([True if chr(num) not in hexString else False for num in list(range(71, 91)) + list(range(103, 123)) ]):
            self.value = int(hexString, 16)
        else:
            raise ValueError('Incorect input data: only hex string')            
    
    def setDec(self, decString):
        if isinstance(decString, str) and decString.isdecimal():
            self.value = int(decString)
        else:
            raise ValueError('Incorect input data: only decimal string')

    def getHex(self):
        return hex(self.value)[2:]
    
    def getDec(self):
      return self.value 

    def __str__(self):
      return  str(self.value)

    def __repr__(self):
      return str(self.value )
      
    def __xor__(self, other):
        return BigNumber(hex(self.value ^ other.value)[2:])
    
    def XOR(self, other):
      return BigNumber(hex(self.value ^ other.value)[2:])

    def __invert__(self):
      return BigNumber(hex(~self.value)[2:])

    def INV(self):
      return BigNumber(hex(~self.value)[2:])

    def OR(self, other):
      return BigNumber(hex(self.value | other.value)[2:])

    def __or__(self):
      return BigNumber(hex(self.value | other.value)[2:])

    def AND(self, other):
      return BigNumber(hex(self.value & other.value)[:2])

    def __and__(self):
      return BigNumber(hex(self.value & other.value)[:2])

    def __lshift__(self, other):
        return BigNumber(hex(self.value << other.value)[:2])

    def shiftL(self, other):
        return BigNumber(hex(self.value << other.value)[2:])        

    def __rshift__(self, other):
        return BigNumber(hex(self.value >> other.value)[2:])
    
    def shiftR(self, other):
        return BigNumber(hex(self.value >> other.value)[2:])        

    def __add__(self, other):
        return BigNumber(hex(self.value + other.value)[2:])

    def ADD(self, other):
        return BigNumber(hex(self.value + other.value)[2:])    

    def __sub__(self, other):
        return BigNumber(hex(self.value - other.value)[2:])

    def SUB(self, other):
        return BigNumber(hex(self.value - other.value)[2:])      

    def __mul__(self, other):
        return BigNumber(hex(self.value * other.value)[2:])

    def MUL(self, other):
        return BigNumber(hex(self.value * other.value)[2:])

    def __mod__(self, other):
      if isinstance(other, BigNumber):
        return BigNumber(hex(self.value % other.value)[2:])
      return BigNumber(hex(self.value % other)[2:])

    def MOD(self, other):
      if isinstance(other, BigNumber):
        return BigNumber(hex(self.value % other.value)[2:])
      return BigNumber(hex(self.value % other)[2:])

    def __div__(self, other):
      if isinstance(other, BigNumber):
        return BigNumber(hex(self.value / other.value)[2:])
      return BigNumber(hex(self.value / other)[2:])

    def DIV(self, other):
      if isinstance(other, BigNumber):
        return BigNumber(hex(self.value / other.value)[2:])
      return BigNumber(hex(self.value / other)[2:])
    

# XOR TEST
a = BigNumber()
b = BigNumber()
a.setHex('51bf608414ad5726a3c1bec098f77b1b54ffb2787f8d528a74c1d7fde6470ea4')
b.setHex('403db8ad88a3932a0b7e8189aed9eeffb8121dfac05c3512fdb396dd73f6331c')
assert a.XOR(b).getHex() == '1182d8299c0ec40ca8bf3f49362e95e4ecedaf82bfd167988972412095b13db8'
assert (a^b).getHex() == '1182d8299c0ec40ca8bf3f49362e95e4ecedaf82bfd167988972412095b13db8'
print('Test XOR pass')

# ADD TEST
a = BigNumber()
b = BigNumber()
a.setHex('36f028580bb02cc8272a9a020f4200e346e276ae664e45ee80745574e2f5ab80')
b.setHex('70983d692f648185febe6d6fa607630ae68649f7e6fc45b94680096c06e4fadb')
res = 'a78865c13b14ae4e25e90771b54963ee2d68c0a64d4a8ba7c6f45ee0e9daa65b'
assert a.ADD(b).getHex() == res
assert (a + b).getHex() == res
print('Test ADD pass')

# SUB TEST
a = BigNumber()
b = BigNumber()
a.setHex('33ced2c76b26cae94e162c4c0d2c0ff7c13094b0185a3c122e732d5ba77efebc')
b.setHex('22e962951cb6cd2ce279ab0e2095825c141d48ef3ca9dabf253e38760b57fe03')
res = '10e570324e6ffdbc6b9c813dec968d9bad134bc0dbb061530934f4e59c2700b9'
assert a.SUB(b).getHex() == res
assert (a - b).getHex() == res
print('Test SUB pass')

# MUL TEST
a = BigNumber()
b = BigNumber()
a.setHex('7d7deab2affa38154326e96d350deee1')
b.setHex('97f92a75b3faf8939e8e98b96476fd22')
res = '4a7f69b908e167eb0dc9af7bbaa5456039c38359e4de4f169ca10c44d0a416e2'
assert a.MUL(b).getHex() == res
assert (a * b).getHex() == res
print('Test MUL pass')

