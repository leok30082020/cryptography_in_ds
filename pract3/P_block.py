import random

def p_block_forward(s: str):
  if not isinstance(s, str):
    raise TypeError('invalid input data: must be str')
    if len(s) != 1:
      raise ValueError('invalid input data: must be 1 char')  
  bits8_repr = f"{ord(s):08b}"
  print(f"Input char: {s} >> ascii code: {ord(s)} >> 8bit representation: {bits8_repr}")
  idx_permut = list(range(8))
  random.shuffle(idx_permut)
  print(f'Permutation table: ', idx_permut)
  res = ''
  for i in idx_permut:
    res += bits8_repr[i]
  print(f"Result 8bit representation: {res}")  
  res_char = int(res, 2)
  print(f'Result ascii: {res_char} >> char: {chr(res_char)}')
  return res, idx_permut, chr(res_char)

def p_block_backward(s: str, idx_permut: list):
  bits8_repr = f"{ord(s):08b}"
  res = [0] * 8
  for i, j in enumerate(idx_permut):
    res[j] = bits8_repr[i]
  res = ''.join(res)
  print(f"Restore (orignal) 8bit representation: {res}")
  res_char = int(res, 2)
  print(f'Restore ascii: {res_char} >> char: {chr(res_char)}')
  return res, chr(res_char)

pb_ff, table, res_char = p_block_forward('a')
restore_8bit_repr, restore_char = p_block_backward(res_char, table)
