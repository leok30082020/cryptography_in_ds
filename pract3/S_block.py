def generate_s_box():
    s_box = [[0] * 16 for _ in range(16)]    
    for row in range(16):        
        for col in range(16):           
            output_value = ((row ^ col) << 4 | row)            
            s_box[row][col] = output_value    
    return s_box

def print_s_box(s_box):
    print('\t' + '    |'.join([f'{i:04b}' for i in range(16)]))
    print('\t' + '='*143)
    for row in range(16):
      print(f'{row:04b}  |', end=' ')
      for col in range(16):
        print(f'{s_box[row][col]:08b}', end=' ')
      print()

def get_S_box_value(s, S_box):
  if isinstance(s, str) and len(s) == 1:    
    bits8_repr = f'{ord(s):08b}'  
    print(f"Input char: {s} >> ascii code: {ord(s)} >> 8bit representation: {bits8_repr}")
  elif isinstance(s, int) and  s >= 0 and s <=255:    
    bits8_repr = f'{s:08b}'  
    print(f"Input num: {s} >> 8bit representation: {bits8_repr}")
  else:
    raise ValueError('invalid input data')
  row, col = bits8_repr[:4], bits8_repr[4:]
  print(f'S_box row: {row}, S_box col: {col}')
  s_box_value = s_box[int(row, 2)][int(col, 2)]
  return s_box_value

def invert_s_box(s_box, value):
    for row in range(16):
        for col in range(16):
            if s_box[row][col] == value:
                orig_input = f'{row:04b}' + f'{col:04b}'
                return int(orig_input, 2)


s_box = generate_s_box()
print("Clalculated S-box:\n" )
print_s_box(s_box)


s_box_val = get_S_box_value('a', s_box)
print(f"S_box value: {s_box_val} >> binary repr: {s_box_val:08b}")
print()
s_box_val2 = get_S_box_value(246, s_box)
print(f"S_box value: {s_box_val2} >> binary repr: {s_box_val2:08b}")


input_value = invert_s_box(s_box, s_box_val2)
print(f"S-box value: {s_box_val2} >> bin repr: {s_box_val2:08b}")
print(f"Orginal S-box input value: {input_value} >> bin repr: {input_value:08b}")
