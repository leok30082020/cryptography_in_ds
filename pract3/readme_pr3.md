### [Pract_3_Google_Collab_Notebook](https://colab.research.google.com/drive/1l14WoCx7mQ6dNnGaMqzXyyiwDfMiQOqR?usp=sharing)

Some tests in collab notebook

## P-block code
Permutation table generates randomly

```python
import random

def p_block_forward(s: str):
  if not isinstance(s, str):
    raise TypeError('invalid input data: must be str')
    if len(s) != 1:
      raise ValueError('invalid input data: must be 1 char')  
  bits8_repr = f"{ord(s):08b}"
  print(f"Input char: {s} >> ascii code: {ord(s)} >> 8bit representation: {bits8_repr}")
  idx_permut = list(range(8))
  random.shuffle(idx_permut)
  print(f'Permutation table: ', idx_permut)
  res = ''
  for i in idx_permut:
    res += bits8_repr[i]
  print(f"Result 8bit representation: {res}")  
  res_char = int(res, 2)
  print(f'Result ascii: {res_char} >> char: {chr(res_char)}')
  return res, idx_permut, chr(res_char)

def p_block_backward(s: str, idx_permut: list):
  bits8_repr = f"{ord(s):08b}"
  res = [0] * 8
  for i, j in enumerate(idx_permut):
    res[j] = bits8_repr[i]
  res = ''.join(res)
  print(f"Restore (orignal) 8bit representation: {res}")
  res_char = int(res, 2)
  print(f'Restore ascii: {res_char} >> char: {chr(res_char)}')
  return res, chr(res_char)

pb_ff, table, res_char = p_block_forward('a')
restore_8bit_repr, restore_char = p_block_backward(res_char, table)
```

### Output
```
Input char: a >> ascii code: 97 >> 8bit representation: 01100001
Permutation table:  [4, 1, 3, 7, 0, 2, 5, 6]
Result 8bit representation: 01010100
Result ascii: 84 >> char: T

Restore (orignal) 8bit representation: 01100001
Restore ascii: 97 >> char: a
```

### Tests
```python
bin1, table1, test_val1 = p_block_forward('b')
assert  p_block_backward(test_val1, table1)[1] == 'b'
assert ord(test_val1) == int(bin1, 2)
print()
bin2, table2, test_val2 = p_block_forward('y')
assert  p_block_backward(test_val2, table2)[1] == 'y'
assert ord(test_val2) == int(bin2, 2)
print("PASS!!!")
```
### Output tests
```
Input char: b >> ascii code: 98 >> 8bit representation: 01100010
Permutation table:  [4, 1, 3, 7, 0, 2, 5, 6]
Result 8bit representation: 01000101
Result ascii: 69 >> char: E
Restore (orignal) 8bit representation: 01100010
Restore ascii: 98 >> char: b

Input char: y >> ascii code: 121 >> 8bit representation: 01111001
Permutation table:  [0, 5, 2, 3, 1, 6, 4, 7]
Result 8bit representation: 00111011
Result ascii: 59 >> char: ;
Restore (orignal) 8bit representation: 01111001
Restore ascii: 121 >> char: y
PASS!!!
```

## B-block
Main logic for getting S-table
- S-table size is 16x16
- S-table = index_row XOR index_col (index_col, index_row - 4bit number
- S-table = S-table LShift 4bits
- S-table = S-table Or index_row

```python
def generate_s_box():
    s_box = [[0] * 16 for _ in range(16)]    
    for row in range(16):        
        for col in range(16):           
            output_value = ((row ^ col) << 4 | row)            
            s_box[row][col] = output_value    
    return s_box

def print_s_box(s_box):
    print('\t' + '    |'.join([f'{i:04b}' for i in range(16)]))
    print('\t' + '='*143)
    for row in range(16):
      print(f'{row:04b}  |', end=' ')
      for col in range(16):
        print(f'{s_box[row][col]:08b}', end=' ')
      print()

def get_S_box_value(s, S_box):
  if isinstance(s, str) and len(s) == 1:    
    bits8_repr = f'{ord(s):08b}'  
    print(f"Input char: {s} >> ascii code: {ord(s)} >> 8bit representation: {bits8_repr}")
  elif isinstance(s, int) and  s >= 0 and s <=255:    
    bits8_repr = f'{s:08b}'  
    print(f"Input num: {s} >> 8bit representation: {bits8_repr}")
  else:
    raise ValueError('invalid input data')
  row, col = bits8_repr[:4], bits8_repr[4:]
  print(f'S_box row: {row}, S_box col: {col}')
  s_box_value = s_box[int(row, 2)][int(col, 2)]
  return s_box_value

def invert_s_box(s_box, value):
    for row in range(16):
        for col in range(16):
            if s_box[row][col] == value:
                orig_input = f'{row:04b}' + f'{col:04b}'
                return int(orig_input, 2)


s_box = generate_s_box()
print("Clalculated S-box:\n" )
print_s_box(s_box)


s_box_val = get_S_box_value('a', s_box)
print(f"S_box value: {s_box_val} >> binary repr: {s_box_val:08b}")
print()
s_box_val2 = get_S_box_value(246, s_box)
print(f"S_box value: {s_box_val2} >> binary repr: {s_box_val2:08b}")


input_value = invert_s_box(s_box, s_box_val2)
print(f"S-box value: {s_box_val2} >> bin repr: {s_box_val2:08b}")
print(f"Orginal S-box input value: {input_value} >> bin repr: {input_value:08b}")
```

### Output
```
Clalculated S-box:

	0000    |0001    |0010    |0011    |0100    |0101    |0110    |0111    |1000    |1001    |1010    |1011    |1100    |1101    |1110    |1111
	===============================================================================================================================================
0000  | 00000000 00010000 00100000 00110000 01000000 01010000 01100000 01110000 10000000 10010000 10100000 10110000 11000000 11010000 11100000 11110000 
0001  | 00010001 00000001 00110001 00100001 01010001 01000001 01110001 01100001 10010001 10000001 10110001 10100001 11010001 11000001 11110001 11100001 
0010  | 00100010 00110010 00000010 00010010 01100010 01110010 01000010 01010010 10100010 10110010 10000010 10010010 11100010 11110010 11000010 11010010 
0011  | 00110011 00100011 00010011 00000011 01110011 01100011 01010011 01000011 10110011 10100011 10010011 10000011 11110011 11100011 11010011 11000011 
0100  | 01000100 01010100 01100100 01110100 00000100 00010100 00100100 00110100 11000100 11010100 11100100 11110100 10000100 10010100 10100100 10110100 
0101  | 01010101 01000101 01110101 01100101 00010101 00000101 00110101 00100101 11010101 11000101 11110101 11100101 10010101 10000101 10110101 10100101 
0110  | 01100110 01110110 01000110 01010110 00100110 00110110 00000110 00010110 11100110 11110110 11000110 11010110 10100110 10110110 10000110 10010110 
0111  | 01110111 01100111 01010111 01000111 00110111 00100111 00010111 00000111 11110111 11100111 11010111 11000111 10110111 10100111 10010111 10000111 
1000  | 10001000 10011000 10101000 10111000 11001000 11011000 11101000 11111000 00001000 00011000 00101000 00111000 01001000 01011000 01101000 01111000 
1001  | 10011001 10001001 10111001 10101001 11011001 11001001 11111001 11101001 00011001 00001001 00111001 00101001 01011001 01001001 01111001 01101001 
1010  | 10101010 10111010 10001010 10011010 11101010 11111010 11001010 11011010 00101010 00111010 00001010 00011010 01101010 01111010 01001010 01011010 
1011  | 10111011 10101011 10011011 10001011 11111011 11101011 11011011 11001011 00111011 00101011 00011011 00001011 01111011 01101011 01011011 01001011 
1100  | 11001100 11011100 11101100 11111100 10001100 10011100 10101100 10111100 01001100 01011100 01101100 01111100 00001100 00011100 00101100 00111100 
1101  | 11011101 11001101 11111101 11101101 10011101 10001101 10111101 10101101 01011101 01001101 01111101 01101101 00011101 00001101 00111101 00101101 
1110  | 11101110 11111110 11001110 11011110 10101110 10111110 10001110 10011110 01101110 01111110 01001110 01011110 00101110 00111110 00001110 00011110 
1111  | 11111111 11101111 11011111 11001111 10111111 10101111 10011111 10001111 01111111 01101111 01011111 01001111 00111111 00101111 00011111 00001111 


Input char: a >> ascii code: 97 >> 8bit representation: 01100001
S_box row: 0110, S_box col: 0001
S_box value: 118 >> binary repr: 01110110

Input num: 246 >> 8bit representation: 11110110
S_box row: 1111, S_box col: 0110
S_box value: 159 >> binary repr: 10011111

S-box value: 159 >> bin repr: 10011111
Orginal S-box input value: 246 >> bin repr: 11110110 
```

### Some tests
### Test1
```python
assert s_box[4][4] == ((4 ^ 4) << 4 | 4)
assert s_box[15][6] == ((15 ^ 6) << 4 | 15)
assert f'{((15 ^ 6) << 4 | 15):08b}' == '10011111'
assert ((15 ^ 6) << 4 | 15) == int('10011111', 2)
assert int('10011111', 2) == 159
print("PASS!!!")
```
### Output
```
PASS!!!
```
### Test2
```python
assert s_box[6][1] == get_S_box_value('a', s_box)
assert s_box[15][6] == get_S_box_value(246, s_box)
print('PASS!!!')
```
### Output
```
Input char: a >> ascii code: 97 >> 8bit representation: 01100001
S_box row: 0110, S_box col: 0001
Input num: 246 >> 8bit representation: 11110110
S_box row: 1111, S_box col: 0110
PASS!!!
```
### Test3
```python
input_s_box_value = 246
assert input_s_box_value == invert_s_box(s_box, get_S_box_value(input_s_box_value, s_box))
print('PASS!!!')
```
### Output
```
Input num: 246 >> 8bit representation: 11110110
S_box row: 1111, S_box col: 0110
PASS!!
```





