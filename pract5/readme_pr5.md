### [Pract_5_Google_Collab_Notebook](https://colab.research.google.com/drive/1naCSaHlcZucAFxdJC1niBvYnW1KTsY3m?usp=sharing)

### Python code for SHA1
```python
import struct
import hashlib

def sha1(message):
    # ������������ ������
    h0 = 0x67452301
    h1 = 0xEFCDAB89
    h2 = 0x98BADCFE
    h3 = 0x10325476
    h4 = 0xC3D2E1F0
    
    # ���������� ������� �����������
    # ��������� �� '1' �� �����������
    message += b'\x80'
    
    # ������ ��������� ������� ����
    while (len(message) * 8) % 512 != 448:
        message += b'\x00'
    
    # ������ ������� ����������� � ������ 64-������ ������ �����
    message += struct.pack('>Q', len(message) * 8)
    
    # ��������� ����������� �� ����� �� 512 ��
    blocks = [message[i:i+64] for i in range(0, len(message), 64)]
    
    # �������� ���������� ���-��������
    for block in blocks:
        # ��������� ���� �� 16 ��� �� 32 ���
        words = list(struct.unpack('>16L', block))
        
        # ���������� ���� �� 80 ���
        for i in range(16, 80):
            words.append(rotate_left((words[i-3] ^ words[i-8] ^ words[i-14] ^ words[i-16]), 1, 32))
        
        # ������������ ������
        a = h0
        b = h1
        c = h2
        d = h3
        e = h4
        
        # �������� ����
        for i in range(80):
            if 0 <= i <= 19:
                f = (b & c) | ((~b) & d)
                k = 0x5A827999
            elif 20 <= i <= 39:
                f = b ^ c ^ d
                k = 0x6ED9EBA1
            elif 40 <= i <= 59:
                f = (b & c) | (b & d) | (c & d)
                k = 0x8F1BBCDC
            elif 60 <= i <= 79:
                f = b ^ c ^ d
                k = 0xCA62C1D6
            
            temp = rotate_left(a, 5, 32) + f + e + k + words[i]
            e = d
            d = c
            c = rotate_left(b, 30, 32)
            b = a
            a = temp
        
        # ������ ���-�������� ����� ����� �� ����������
        h0 = (h0 + a) & 0xFFFFFFFF
        h1 = (h1 + b) & 0xFFFFFFFF
        h2 = (h2 + c) & 0xFFFFFFFF
        h3 = (h3 + d) & 0xFFFFFFFF
        h4 = (h4 + e) & 0xFFFFFFFF

    # ������������ ���-������� � big-endian
    digest = struct.pack('>5L', h0, h1, h2, h3, h4)

    return digest

def rotate_left(n, d, bits):
   return ((n << d) | (n >> (bits - d))) & ((1 << bits) - 1)

message = "Hello, world!"
print(f'Message for hashing: {message}')
sha1_my = sha1(message.encode()).hex()
print('Digest with own realization: ', sha1_my)
sha1_lib = hashlib.sha1(message.encode()).hexdigest()
print('Digest with lib realization: ', sha1_lib)
print('[+] Digests matched by value' if sha1_my == sha1_lib else '[-] Digests not matched by value')
print('[+] Digests matched by length' if len(sha1_my) == len(sha1_lib) else '[-] Digests not matched by length')
```

### Output
```Message for hashing: Hello, world!
Digest with own realization:  f62c249e5b2e985281d2bb504b67187c9a2a01b4
Digest with lib realization:  943a702d06f34599aee1f8da8ef9f7296031d699
[-] Digests not matched by value
[+] Digests matched by length
```
### Time for own realization
```
152 �s � 54.8 �s per loop (mean � std. dev. of 7 runs, 10000 loops each)
```
### Time for lib realization
```
956 ns � 315 ns per loop (mean � std. dev. of 7 runs, 1000000 loops each)
```

### Lib realizatiom faster almost in 159 times