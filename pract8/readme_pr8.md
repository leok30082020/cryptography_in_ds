### [Pract_8_Google_Collab_Notebook](https://colab.research.google.com/drive/1CCAWFWQbnWRTt-6hIE_sLzB4Ibi9lzY1?usp=sharing)


### Description
```
Клас EllipticCurve є обгорткою над бібліотекою cryptography для роботи з еліптичними кривими. Цей клас надає зручний інтерфейс для генерації ключевих пар, виконання перетворень у групі точок еліптичної кривої, експорту та імпорту ключів, підписування та перевірки підпису повідомлень.
Використовуються еліптичні криві `secp256r1` та `secp384r1` (за замовучанням).

Ось опис всіх методів класу EllipticCurve:

Метод __init__(self, curve_name):

Приймає назву еліптичної кривої curve_name.
Ініціалізує об'єкт класу EllipticCurve з вказаною назвою кривої.
Метод generate_key_pair(self):

Генерує ключову пару (приватний ключ і відповідний публічний ключ) для еліптичної кривої.
Повертає приватний ключ і публічний ключ у форматі, придатному для подальшої обробки.
Метод multiply_point(self, point, multiplier):

Виконує перетворення в групі точок еліптичної кривої, в результаті якого вказана точка point множиться на скалярний множник multiplier.
Повертає отриману точку.
Метод export_private_key(self, private_key, password=None):

Експортує приватний ключ у форматі PEM.
При необхідності можна вказати пароль password, який зашифрує приватний ключ.
Повертає приватний ключ у форматі PEM.
Метод export_public_key(self, public_key):

Експортує публічний ключ у форматі PEM.
Повертає публічний ключ у форматі PEM.
Метод import_private_key(self, private_key_pem, password=None):

Імпортує приватний ключ з PEM-формату.
Якщо ключ був зашифрований, можна передати пароль password, щоб розшифрувати його.
Повертає імпортований приватний ключ.
Метод import_public_key(self, public_key_pem):

Імпортує публічний ключ з PEM-формату.
Повертає імпортований публічний ключ.
Метод sign_message(self, private_key, message):

Підписує повідомлення message за допомогою приватного ключа private_key.
Використовує алгоритм ECDSA з хешуванням SHA-256.
Повертає підписане повідомлення.
Метод verify_signature(self, public_key, message, signature):

Перевіряє підпис signature для повідомлення message з використанням публічного ключа public_key.
Якщо підпис є валідним, повертає True; в іншому випадку - False.
```

### Python code for using Elliptic Curve Cryptography with `cryptography`-library
```python
## isntall "cryptography" libraty before

from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePrivateKey, EllipticCurvePublicKey
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes

class EllipticCurve:
    def __init__(self, curve_name):
        self.curve_name = curve_name

    def generate_key_pair(self):
        private_key = ec.generate_private_key(
            ec.SECP256R1() if self.curve_name == 'secp256r1' else ec.SECP384R1(),
        )
        public_key = private_key.public_key()
        return private_key, public_key

    def export_private_key(self, private_key, password=None):
        private_key_pem = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.BestAvailableEncryption(password) if password else serialization.NoEncryption()
        )
        return private_key_pem

    def export_public_key(self, public_key):
        public_key_pem = public_key.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo
        )
        return public_key_pem

    def import_private_key(self, private_key_pem, password=None):
        private_key = serialization.load_pem_private_key(
            private_key_pem,
            password=password
        )
        return private_key

    def import_public_key(self, public_key_pem):
        public_key = serialization.load_pem_public_key(public_key_pem)
        return public_key

    def sign_message(self, private_key, message):
        signature = private_key.sign(
            message,
            ec.ECDSA(hashes.SHA256())
        )
        return signature

    def verify_signature(self, public_key, message, signature):
        try:
            public_key.verify(
                signature,
                message,
                ec.ECDSA(hashes.SHA256())
            )
            return True
        except Exception:
            return False

# Приклад використання
curve = EllipticCurve('secp256r1')
private_key, public_key = curve.generate_key_pair()

print("Приватний ключ :")
print(curve.export_private_key(private_key).decode())
print("Публічний ключ:")
print(curve.export_public_key(public_key).decode())

message = b"Hello, world!"
print(f'Повідомлення: ', message)
signature = curve.sign_message(private_key, message)
print("Підпис:", signature.hex())

is_valid = curve.verify_signature(public_key, message, signature)
print("Перевірка підпису:", is_valid)

print('==========================================================')
curve2 = EllipticCurve('secp256r1')
private_key2, public_key2 = curve2.generate_key_pair()

print("Приватний ключ 2:")
pr2 = curve2.export_private_key(private_key2)
print(pr2.decode())
print("Публічний ключ 2:")
pub2 = curve2.export_public_key(public_key2)
print(pub2.decode())

message2 = b"Hello, Cryptography!"
print(f'Повідомлення 2: ', message2)
signature2 = curve2.sign_message(private_key2, message2)
print("Підпис:", signature2.hex())

is_valid2 = curve.verify_signature(public_key2, message2, signature2)
print("Перевірка підпису 2:", is_valid2)

print('==========================================================')
print('Імпорт іншого публічного ключа і перевірка підпису: ')
imp_pub2 = curve.import_public_key(pub2)
print(f'Повідомлення: ', message2)
print(f'Підпис на основі імпортованого ключа: \n {signature2.hex()}')
print('Перевірка підпису: ', 'Верифіковано' if curve.verify_signature(imp_pub2, message2, signature2) else 'Не верифіковано')

```

### Output
```
Приватний ключ :
-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgAAbckdKxz5fDthNL
1RyfXLGd90w22/G0CJ5J6LpCRg+hRANCAAQwn/G51n0DEY2F+Q9OiPkM/h0FOciv
+ifOkFUoL9Rt97raGDCbgLUD6aac+FtdSfNJghI0FcCiN3SfBrXI8t5X
-----END PRIVATE KEY-----

Публічний ключ:
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEMJ/xudZ9AxGNhfkPToj5DP4dBTnI
r/onzpBVKC/Ubfe62hgwm4C1A+mmnPhbXUnzSYISNBXAojd0nwa1yPLeVw==
-----END PUBLIC KEY-----

Повідомлення:  b'Hello, world!'
Підпис: 304402200ed179ffb921211abc25dcd37df0b74a8bb48af36b8e2288e63d68a389eb24b5022001b9632724be44aedaf725bcf5d3210949aa84985afe52fdf011a3160c1e127d
Перевірка підпису: True
==========================================================
Приватний ключ 2:
-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQg5/js6xduPFQMmXkd
PFittGawWvZZgYXkfxxEacjR/NOhRANCAARfZ0003L2CLGsaSqhmbKSVCVMvVCBf
Pf1bVQm8tXdMDcdf8bneTqxNIPmFf7XpAx6xNOLrl1hX4PvfKTtQQYkV
-----END PRIVATE KEY-----

Публічний ключ 2:
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEX2dNNNy9gixrGkqoZmyklQlTL1Qg
Xz39W1UJvLV3TA3HX/G53k6sTSD5hX+16QMesTTi65dYV+D73yk7UEGJFQ==
-----END PUBLIC KEY-----

Повідомлення 2:  b'Hello, Cryptography!'
Підпис: 3046022100b7d1df445bf42c3372899b6c0215c8a30f0fa9cab34aea6aef8c5542c53f73ff022100f098dcdc6bc938cf6c868c055cc6a9d9d309fd6e2179d3db621512af9fbe2149
Перевірка підпису 2: True
==========================================================
Імпорт іншого публічного ключа і перевірка підпису: 
Повідомлення:  b'Hello, Cryptography!'
Підпис на основі імпортованого ключа: 
 3046022100b7d1df445bf42c3372899b6c0215c8a30f0fa9cab34aea6aef8c5542c53f73ff022100f098dcdc6bc938cf6c868c055cc6a9d9d309fd6e2179d3db621512af9fbe2149
Перевірка підпису:  Верифіковано
```